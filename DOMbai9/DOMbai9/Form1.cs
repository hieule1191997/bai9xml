﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace DOMbai9
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        XmlDocument doc = new XmlDocument();
        public string pathbangdiem = "../../bangdiem.xml";
        public string pathmonhoc = "../../monhoc.xml";
        public string paththongtinsv = "../../XMLthongtinSV.xml";

        public void hienthi()
        {
            DataSet dts = new DataSet();
            dts.ReadXml(pathbangdiem);
            dataGridView1.DataSource = dts.Tables["sinhvien"];
        }

        public void loadcombobox()
        {
            DataSet dtsmonhoc = new DataSet();
            dtsmonhoc.ReadXml(pathmonhoc);
            DataSet dtssv = new DataSet();
            dtssv.ReadXml(paththongtinsv);
            cbbmasv.DataSource = dtssv.Tables["sinhvien1"];
            cbbmasv.DisplayMember = "masinhvien1";
            cbbmonhoc.DataSource = dtsmonhoc.Tables["monhoc"];
            cbbmonhoc.DisplayMember = "tenmonhoc";

        }

        public string tensinhvien( string masv)
        {
            doc.Load(paththongtinsv);
            XmlNode node = doc.SelectSingleNode("/thongtinsv/sinhvien1[masinhvien1 = '" + masv.Trim() + "']");
            string hotensv = node.ChildNodes[1].InnerText.ToString();
            return hotensv;
        }
        // them 1 sinh vien vao xml bang diem
        public void them()
        {
            string tensv = tensinhvien(cbbmasv.Text);
            doc.Load(pathbangdiem);
            XmlElement diemlan1,diemlan2,sinhvien,hoten;
            XmlAttribute masinhvien,monhoc;
            
            sinhvien = doc.CreateElement("sinhvien");
            masinhvien = doc.CreateAttribute("masv");
            monhoc = doc.CreateAttribute("monhoc");
            hoten = doc.CreateElement("hoten");
            diemlan1 = doc.CreateElement("diemlan1");
            diemlan2 = doc.CreateElement("diemlan2");
            

            masinhvien.InnerText = cbbmasv.Text;
            monhoc.InnerText = cbbmonhoc.Text;
            hoten.InnerText =tensv;
            diemlan1.InnerText = txbdiem1.Text;
            diemlan2.InnerText = txbdiem2.Text;

            sinhvien.SetAttributeNode(masinhvien);
            sinhvien.SetAttributeNode(monhoc);
            sinhvien.AppendChild(hoten);
            sinhvien.AppendChild(diemlan1);
            sinhvien.AppendChild(diemlan2);

            doc.DocumentElement.AppendChild(sinhvien);

            doc.Save(pathbangdiem);
            MessageBox.Show("Bạn đã thêm thành công thông tin", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

        }
        //sua thong tin sinh vien 
        public void sua()
        {
            doc.Load(pathbangdiem);
            XmlNode node = doc.SelectSingleNode("/bangdiemsv/sinhvien[@masv = '" + (cbbmasv.Text).Trim() + "']");
            if (node != null)
            {
                node.Attributes[1].InnerText = cbbmonhoc.Text;
                node.ChildNodes[1].InnerText = txbdiem1.Text;
                node.ChildNodes[2].InnerText = txbdiem2.Text;
                doc.Save(pathbangdiem);
                MessageBox.Show("Bạn đã sửa thông tin thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {

                MessageBox.Show("Thong tin muốn sửa không có trong dữ liệu ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        // xoa 1 bang diem sinh vien 
        public void xoa()
        {
            doc.Load(pathbangdiem);
            XmlNode node = doc.SelectSingleNode("/bangdiemsv/sinhvien[@masv = '" + (cbbmasv.Text).Trim() + "']");
            if (node != null)
            {
                doc.DocumentElement.RemoveChild(node);
                doc.Save(pathbangdiem);
                MessageBox.Show("Bạn đã xoá thông tin thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {

                MessageBox.Show("thong tin muốn sửa không có trong dữ liệu ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            hienthi();
            loadcombobox();
        }

        private void btnthem_Click(object sender, EventArgs e)
        {
           
            try
            {
                if(cbbmasv.Text =="" || cbbmonhoc.Text == "" || txbdiem1.Text == "" || txbdiem2.Text == "")
                {
                    MessageBox.Show("Vui lòng nhập đầy đủ thông tin", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    them();
                    hienthi();
                }
            }
            catch
            {
                MessageBox.Show("Có lỗi xảy ra không thể thêm", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnsua_Click(object sender, EventArgs e)
        {

            try
            {
                if (cbbmasv.Text == "")
                {
                    MessageBox.Show("Vui lòng nhập mã sinh viên muốn sửa", "thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    sua();
                    hienthi();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("có lỗi xảy ra không thể sửa", "thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnxoa_Click(object sender, EventArgs e)
        {
            try
            {
                if (cbbmasv.Text == "")
                {
                    MessageBox.Show("Vui lòng nhập ma sinh vien muốn xoá", "thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    xoa();
                    hienthi();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("có lỗi xảy ra không thể xóa", "thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnthoat_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Bạn có thự sự muốn thoát chương trình ?", "thông báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            if (dr == DialogResult.OK)
            {
                Application.Exit();

            }
        }

        private void dataGridView1_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            int i = e.RowIndex;
            cbbmasv.Text = dataGridView1.Rows[i].Cells[3].Value.ToString();
            cbbmonhoc.Text = dataGridView1.Rows[i].Cells[4].Value.ToString();
            txbdiem1.Text = dataGridView1.Rows[i].Cells[1].Value.ToString();
            txbdiem2.Text = dataGridView1.Rows[i].Cells[2].Value.ToString();
        }
    }
}
